let defaultState = {
  title: 'Заголовок формы',
  rknText: 'Нажимая кнопку отправить, я даю согласие на обработку персональных данных.',
  rknLink: {
    'url': '#',
    'text': 'Условия использования данных',
    'download': true, 
  },
  status: 'pending', // 'pending' | 'ready' will activate button | 'sended' will show result of sending
  result: [
    { type: 'icon', content: 'success' },
    { type: 'p', content: 'Заявка отправлена' }, 
    { type: 'p', content: 'Менеджер уже звонит, посмотрите на телефон' },
  ],
  fields: {
    name: {
      id: 'name',
      value: '',
      required: true, // empty value mean error of form
      type: 'text', // input type
      validation: '', // '' | success | error - used in classess for design of status
      focus: false, // focus status
      messages: {
        regular: 'Имя*', // will show in usual appear
        error: 'Только русские буквы и проблемы', // will show with error regular check
        good: 'Вас красиво зовут', // will show with successful regular check
      },
      regexp: /^[а-яА-ЯёЁa\s]+$/, 
    },
    phone: {
      id: 'phone',
      value: '',
      required: true,
      type: 'text',
      validation: '',
      focus: false,
      messages: {
        regular: 'Номер телефона*',
        error: 'Только цифры и + вначале, всего 11 цифр',
        good: 'Теперь мы точно дозвонимся',
      },
      regexp: /^\+{0,1}[0-9]{11}$/,
    },
    mail: {
      id: 'mail',
      value: '',
      required: false,
      type: 'email',
      validation: '',
      focus: false,
      messages: {
        regular: 'Электронная почта',
        error: 'Нужна правильная электронная почта',
        good: 'После звонка и КП вышлем на почту'
      },
      regexp: /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/
    },
  }
}

const formReducer = (state = defaultState, action) => {
  let newState = {};
  switch(action.type) {
    case "SET_FIELD_STATE":
      newState = {
        fields: Object.assign( {}, state.fields, {
          [action.data.field]: Object.assign( {}, state.fields[action.data.field], { 
            [action.data.stateName] : action.data.stateVal
          } ),
        } ),
      };
      break;

    case "SET_FIELD_VAL":
      newState = {
        fields: Object.assign( {}, state.fields, {
          [action.data.field]: Object.assign( {}, state.fields[action.data.field], { 
            [action.data.stateName] : action.data.stateVal
          } ),
        } ),
      };
      let validation = true;
      for ( var field_name in newState.fields ) {
        if( newState.fields[field_name].required ){
          validation = newState.fields[field_name].value ? true : false; // component send only valid value here, so if it isn't empty - it's valid
        }
        if( !validation ){ // no need check if one of field not valid
          break;
        }
      }
      newState.status = validation ? 'ready': 'pending';
      break;
    
    case "SUBMIT_FORM":
      // send data to server and it can return data for result block
      if( state.status == 'ready' ){
        newState.status = 'sended';
      }
      break;
    default: 
      return state;
  }
  return Object.assign({}, state, newState);
}


export default formReducer;