import React, { Component } from "react";
import { connect } from 'react-redux';
import './Input.less';

const Input = props => {

  const classes = {
    span : 'input__placeholder',
    input: 'input__input',
  };
  let getClass = ( tag ) => {
    let main = classes[tag];
    let all = [];
    all.push( main );
    if( props.field.focus ){
      all.push( main + "_focus" );
    }
    if( props.field.validation ){
      all.push( main + "_" + props.field.validation );
    }
    return all.join(" ");
  };

  const getClassInput = () => {
    return getClass('input');
  };
  const getClassSpan = () => {
    getClass('span');
  };

  const onBlur = (e) => {
    if( !e.target.value ){
      props.updateState( 'SET_FIELD_STATE', props.field.id, 'focus', false );
    }
  };
  const onFocus = (e) => {
    props.updateState( 'SET_FIELD_STATE', props.field.id, 'focus', true );
  };


  const doValidation = (e) => {
    let regRes = false,
      validation = '';
    if( e.target.value ){
      if ( props.field.regexp ){
        regRes = props.field.regexp.test( e.target.value );
        validation = ( regRes ) ? 'success' : 'error';
      } else {
        validation = '';
      }
    } else {
      validation = '';
    }
    
    props.updateState( 'SET_FIELD_VAL', props.field.id, 'value', ( regRes ) ? e.target.value : '' );
    props.updateState( 'SET_FIELD_STATE', props.field.id, 'validation', validation );
  };
      

  const getPlaceholder = () => {
    if( props.field.validation == 'success' ){
      return props.field.messages.good;
    }
    if( props.field.validation == 'error' ){
      return props.field.messages.error;
    }
    return props.field.messages.regular;
  }

  return (
    <div className="input form__row">
      <input 
        className={getClass('input')} 
        name={props.name}
        onBlur={onBlur} 
        onFocus={onFocus} 
        onChange={doValidation}
        type={props.field.type} />
      <span className={getClass('span')}>{getPlaceholder()}</span>
    </div>
  );
}

export default Input;