import React, { Component } from "react";

import './Result.less';
import { connect } from 'react-redux';

const Result = props => {
  if( props.status != 'sended' ){
    return false;
  };
  return (
    <div className="form-result">
    {
      props.result.map(function(item, index) {
        if( item.type == 'icon'){
          return (<span key={index} className={"form-result__icon form-result__icon_"+item.content}></span>)
        }
        if( item.type == 'p'){
          return (<p key={index}>{item.content}</p>)
        }
      })
    }
    </div>
  );
}

const mapStateToProps = (state) => {
  return state;
}
export default connect(mapStateToProps)(Result);