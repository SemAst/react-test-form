import React, { Component } from "react";
import './App.less';
import Form from '../Form/Form';
import Result from '../Result/Result';


const App = props => {

  return (
    <div>
      <Form />
      <Result />
    </div>
  );
}

export default App;