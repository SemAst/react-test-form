import React from "react";
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import {createStore} from 'redux';

import App from "./components/App/App";

import formReducer from './reducers/formReducer';

const store = createStore( formReducer );

ReactDOM.render(
<Provider store={store}>
  <App />
</Provider>
,document.getElementById("app")
);