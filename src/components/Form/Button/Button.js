import React, { Component } from "react";
import './Button.less';


const Button = props => {
  let className = "button form__button";
  if( props.disabled ){
    className += ' button_disabled';
  }
  return (
    <button className={className} disabled={props.disabled}>Начать работу</button>
  );
};
export default Button;