
import formReducer from '../../reducers/formReducer';

describe("Reducer test", () => {
      
  test("change form status after submit with status ready", () => {
    let defaultState = {
      status: 'ready'
    };
    let action = {
      type: "SUBMIT_FORM",
      data: true,
    }
    let resultState = {
      status: 'sended'
    };

    expect(formReducer( defaultState, action )).toEqual(resultState);
  });

  test("change form status after submit with status pending", () => {
    let defaultState = {
      status: 'pending'
    };
    let action = {
      type: "SUBMIT_FORM",
      data: true,
    }
    let resultState = {
      status: 'pending'
    };

    expect(formReducer( defaultState, action )).toEqual(resultState);
  });

  test("add last needed value of fields", () => {
    let defaultState = {
      status: 'pending',
      fields: {
        name: {
          id: 'name',
          value: 'Славное имя',
          required: true,
          type: 'text',
          validation: 'success',
          focus: false,
          messages: {
            regular: 'Имя*',
            error: 'Только русские буквы и проблемы',
            good: 'Вас красиво зовут',
          },
          regexp: /^[а-яА-ЯёЁa\s]+$/, 
        },
        phone: {
          id: 'phone',
          value: '',
          required: true,
          type: 'text',
          validation: 'success',
          focus: false,
          messages: {
            regular: 'Номер телефона*',
            error: 'Только цифры и + вначале, всего 11 цифр',
            good: 'Теперь мы точно дозвонимся',
          },
          regexp: /^\+{0,1}[0-9]{11}$/,
        },
      }
    };

    let action = {
      type: "SET_FIELD_VAL",
      data: {
        field: 'phone',
        stateName: 'value',
        stateVal: '+79991234567',
      },
    };

    let resultState = {
      status: 'ready',
      fields: {
        name: {
          id: 'name',
          value: 'Славное имя',
          required: true,
          type: 'text',
          validation: 'success',
          focus: false,
          messages: {
            regular: 'Имя*',
            error: 'Только русские буквы и проблемы', 
            good: 'Вас красиво зовут',
          },
          regexp: /^[а-яА-ЯёЁa\s]+$/, 
        },
        phone: {
          id: 'phone',
          value: '+79991234567',
          required: true,
          type: 'text',
          validation: 'success',
          focus: false,
          messages: {
            regular: 'Номер телефона*',
            error: 'Только цифры и + вначале, всего 11 цифр',
            good: 'Теперь мы точно дозвонимся',
          },
          regexp: /^\+{0,1}[0-9]{11}$/,
        },
      }
    };

    expect(formReducer( defaultState, action )).toEqual(resultState);
  });
});