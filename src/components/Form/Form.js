import React, { Component } from "react";

import './Form.less';
import { connect } from 'react-redux';

import Input from './Input/Input';
import Button from './Button/Button';


const Rkn = props => {
  return (
    <p className="text form__rkn">{props.text} <a href={props.link.url} download={props.link.download}>{props.link.text}</a>.</p>
  );
}

const Form = props => {
  if( props.status == 'sended' ){
    return false;
  };

  const onSubmit = e => {
    e.preventDefault();
    props.dispatch({
      type: 'SUBMIT_FORM'
    });
    return false;
  };

  const updateState = ( mode, field, stateName, stateVal ) => {
    props.dispatch({
      type: mode,
      data: {
        field,
        stateName,
        stateVal,
      }
    });
  }

  return (
    <form onSubmit={onSubmit} className="form">
      <h2 className="form__title">{props.title}</h2>
      {Object.keys(props.fields).map( fldName => (
        <Input
          name={fldName}
          updateState={updateState}
          field={props.fields[fldName]} />
      ))}
      <Button disabled={(props.status == 'pending') ? true : false}/>
      <Rkn text={props.rknText} link={props.rknLink}/>
    </form>
  );
}

const mapStateToProps = (state) => {
  return state;
}
export default connect(mapStateToProps)(Form);